install.packages(c("remotes", "attachment", "tinytex"), repos = "https://cloud.r-project.org/")

# Latex
tinytex::install_tinytex()

# Gitbook theme
remotes::install_gitlab("dreal-datalab/drealdown")
remotes::install_github("hadley/emo")
remotes::install_github("spyrales/gouvdown")

# Find dependencies
imports <- unique(c(
  "bookdown", "drealdown", "gouvdown",
  "emo",
  # Calls in `r code`
  "knitr",
  attachment::att_from_rmds(".", recursive = FALSE))
)
# update DESCRIPTION file
attachment::att_to_desc_from_is(path.d = "DESCRIPTION", imports = imports)

# Install dependencies
remotes::install_deps(dependencies = TRUE)

